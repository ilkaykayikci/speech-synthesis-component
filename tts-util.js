
var synth = window.speechSynthesis;


export function speak(text, selectedVoice){

    var voices = [];
    voices = synth.getVoices();    
    
    var toSpeak = new SpeechSynthesisUtterance(text);  
    
    voices.forEach((voice)=>{
        if(voice.name === selectedVoice){
            toSpeak.voice = voice;
        }
    });
    
    synth.speak(toSpeak);
}





