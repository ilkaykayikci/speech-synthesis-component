# speech-synthesis-component

A web component which synthesizes speech.

# How to use

Use either yarn or npm to install.

yarn add https://gitlab.com/ilkaykayikci/speech-synthesis-component.git

or

npm install https://gitlab.com/ilkaykayikci/speech-synthesis-component.git --save
