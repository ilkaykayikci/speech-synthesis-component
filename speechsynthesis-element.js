/**
 * @license
 * Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */

import {LitElement, html, css} from 'lit-element';
import {speak} from './tts-util';
/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class SpeechSynthesis extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
    `;
  }

  static get properties() {
    return {
      text: {type: String},
      voices: {type: Array},     
    };
  }

  constructor() {
    super();
    this.text = 'Let it speak!';
    this.voices = [];
    this.voices = window.speechSynthesis.getVoices(); 
    window.speechSynthesis.addEventListener("voiceschanged", () => {
    this.voices = window.speechSynthesis.getVoices();  
      
    })
    
  }

  render() {   
    return html`
    
      Stimme auswählen: 
     
      <select id='voiceList'>${this.voices.map((item) => 
      
       html` <option value=${item.name}> ${item.name}</option>`)}
      </select>
      
      <br> 
      <input type="text" value=${this.text} id="txtinput">
      <button @click=${this._onClick} part="button">
        Sprache ausgeben!
      </button>    
    `;
  }

  _onClick() {
    var txtInput = this.shadowRoot.getElementById('txtinput');
    var voiceList = this.shadowRoot.getElementById('voiceList');

    var selectedVoice = voiceList.options[voiceList.selectedIndex].text;

    speak(txtInput.value, selectedVoice);
  }
} 
customElements.define('speech-synthesis', SpeechSynthesis);
